DROP SCHEMA IF EXISTS `test`;
CREATE SCHEMA `test` DEFAULT CHARACTER SET utf8;
USE test;

    
CREATE TABLE grad (
	id INT NOT NULL AUTO_INCREMENT,
    naziv VARCHAR(20) NOT NULL,
    ptt INT NOT NULL,
   
    PRIMARY KEY(id)
);
    
CREATE TABLE k_manif (
	id INT NOT NULL AUTO_INCREMENT,
    naziv VARCHAR(20) NOT NULL,
    br_pos INT NOT NULL,
    grad_id INT NOT NULL,
    
	PRIMARY KEY(id),
    FOREIGN KEY(grad_id) REFERENCES grad(id)
		ON DELETE RESTRICT
);

INSERT INTO grad (naziv, ptt) VALUES ('Šabac', 15000);
INSERT INTO grad (naziv, ptt) VALUES ('Novi Sad', 21000);
INSERT INTO grad (naziv, ptt) VALUES ('Beograd', 11000);

INSERT INTO k_manif (naziv, br_pos, grad_id) VALUES ('ŠLF', 10000, 1);
INSERT INTO k_manif (naziv, br_pos, grad_id) VALUES ('EXIT', 150000, 2);
INSERT INTO k_manif (naziv, br_pos, grad_id) VALUES ('BEER FEST', 100000, 3);
