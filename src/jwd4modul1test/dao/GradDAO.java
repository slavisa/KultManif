package jwd4modul1test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jwd4modul1test.model.Grad;

public class GradDAO {

	public static ArrayList<Grad> getAll(Connection conn) {
		ArrayList<Grad> gradovi = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM grad";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int ptt = rset.getInt(index++);
				
				Grad grad= new Grad(id, naziv, ptt);
				gradovi.add(grad);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return gradovi;
	}

	public static Grad getByID(Connection conn, int grID) {
		Grad grad=null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM grad WHERE id="+grID;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int ptt = rset.getInt(index++);
				
				grad= new Grad(id, naziv, ptt);
				
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return grad;
	}

	public static boolean update(Connection conn, Grad grad) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE grad SET naziv = ?, ptt = ? WHERE id  = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, grad.getNaziv());
			pstmt.setInt(index++, grad.getPtt());
			pstmt.setInt(index++, grad.getId());
			
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static boolean delete(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM grad WHERE id = " + id;

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	
	}
}
