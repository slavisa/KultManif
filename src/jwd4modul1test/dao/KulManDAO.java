package jwd4modul1test.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import jwd4modul1test.model.Grad;
import jwd4modul1test.model.KulturnaManifestacija;
import jwd4modul1test.ui.ApplicationUI;

public class KulManDAO {
	public static Connection conn = ApplicationUI.getConn();

	public static ArrayList<KulturnaManifestacija> getAll(Connection conn) {
		ArrayList<KulturnaManifestacija> manif = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM k_manif";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brPos = rset.getInt(index++);
				int grID = rset.getInt(index++);

				Grad grad = GradDAO.getByID(conn, grID);
				KulturnaManifestacija kManif = new KulturnaManifestacija(id, naziv, brPos, grad);

				manif.add(kManif);
			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return manif;
	}

	public static KulturnaManifestacija getByID(Connection conn2, int id) {
		KulturnaManifestacija kM = null;
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM k_manif WHERE id=" + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idM = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brPos = rset.getInt(index++);
				int idG = rset.getInt(index++);

				Grad grad = GradDAO.getByID(conn2, idG);

				kM = new KulturnaManifestacija(idM, naziv, brPos, grad);

			}
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return kM;
	}

	public static boolean promeniIme(Connection conn2, String ime, int id) {

		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE k_manif SET naziv = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, ime);
			pstmt.setInt(index++, id);

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException ex1) {
				ex1.printStackTrace();
			}
		}

		return false;
	}

	public static boolean add(Connection conn2, KulturnaManifestacija kM) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO k_manif (naziv, br_pos, grad_id) VALUES (?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, kM.getNaziv());
			pstmt.setInt(index++, kM.getBrPosetilaca());
			pstmt.setInt(index++, kM.getGrad().getId());
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static boolean update(Connection conn2, KulturnaManifestacija kM) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE k_manif SET naziv = ?, br_pos = ?, grad_id = ? WHERE id  = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, kM.getNaziv());
			pstmt.setInt(index++, kM.getBrPosetilaca());
			pstmt.setInt(index++, kM.getGrad().getId());
			pstmt.setInt(index++, kM.getId());
			
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static boolean delete(Connection conn2, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM k_manif WHERE id = " + id;

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

}
