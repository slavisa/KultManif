package jwd4modul1test.ui;

import java.sql.Connection;
import java.util.ArrayList;

import jwd4modul1test.dao.GradDAO;
import jwd4modul1test.dao.KulManDAO;
import jwd4modul1test.model.Grad;
import jwd4modul1test.model.KulturnaManifestacija;
import jwd4modul1test.util.PomocnaKlasa;

public class KulManUI {
	public static Connection conn = ApplicationUI.getConn();

	public static void PrikazManif() {
		ArrayList<KulturnaManifestacija> kManif = new ArrayList<>();
		kManif = KulManDAO.getAll(conn);
		System.out.println();
		for (KulturnaManifestacija kM : kManif) {

			System.out.println(kM.toString());
		}
		System.out.println();
	}

	public static void pretragaPoID() {
		System.out.println("Unesite id kulturne manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		KulturnaManifestacija kM = KulManDAO.getByID(conn, id);
		if (kM != null) {
			System.out.println(kM.toString());
		}
		System.out.println();
	}

	public static void izmenaNaziva() {
		System.out.println("Unesite id kulturne manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		KulturnaManifestacija kM = KulManDAO.getByID(conn, id);
		if (kM != null) {
			System.out.println(kM.toString());
			System.out.println();
			System.out.println("Unesite novo ime za traženu manifestaciju:");
			String ime = PomocnaKlasa.ocitajTekst();
			if (KulManDAO.promeniIme(conn, ime, id)) {
				System.out.println("Ime je promenjeno");
			} else
				System.out.println("Došlo je do problema sa promenom imena?");
		} else
			System.out.println("Ne postoji manifestacija sa unesenim ID");

	}

	public static void unosManif() {
		System.out.println("Unesite naziv manifestacije:");
		String naziv = PomocnaKlasa.ocitajTekst();
		System.out.println("Unesite broj posetilaca:");
		int brPos = PomocnaKlasa.ocitajCeoBroj();
		System.out.println("Unesite id broj grada gde se održava manifestacija:");
		int idGrada = PomocnaKlasa.ocitajCeoBroj();
		Grad grad = GradDAO.getByID(conn, idGrada);
		KulturnaManifestacija kM = new KulturnaManifestacija(0, naziv, brPos, grad);
		if (KulManDAO.add(conn, kM)) {
			System.out.println("Manifestacija je uspešno dodata");
		} else
			System.out.println("Bilo je problema pri dodavanju manifestacije?");
	}

	public static void brisanjeManif() {
		System.out.println("Unesite id broj manifestacije koju želite da izbrišete:");
		int id=PomocnaKlasa.ocitajCeoBroj();
		if(KulManDAO.delete(conn,id)==true) {
			System.out.println("Manifestacija je uspešno izbrisana");
		}else System.out.println("Došlo je do problema prilikom brisanja?");
	}

	public static void izmenaSvihPod() {
		System.out.println("Unesite id kulturne manifestacije:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		KulturnaManifestacija kM = KulManDAO.getByID(conn, id);
		if (kM != null) {
			System.out.println(kM.toString());
			System.out.println();
			System.out.println("Unesite novo ime za traženu manifestaciju:");
			String ime = PomocnaKlasa.ocitajTekst();
			System.out.println("Unesite novi broj posetilaca:");
			int brPos = PomocnaKlasa.ocitajCeoBroj();
			System.out.println("Unesite novi id broj grada gde se održava manifestacija:");
			int idGrada = PomocnaKlasa.ocitajCeoBroj();
			Grad grad = GradDAO.getByID(conn, idGrada);
			kM = new KulturnaManifestacija(id, ime, brPos, grad);
			if (KulManDAO.update(conn, kM)==true) {
				System.out.println("Podaci su uspešno promenjeni");
			} else
				System.out.println("Došlo je do problema sa promenom ipodataka");
		} else
			System.out.println("Ne postoji manifestacija sa unesenim ID");

	}

}
