package jwd4modul1test.ui;

import java.sql.Connection;
import java.util.ArrayList;

import jwd4modul1test.dao.GradDAO;
import jwd4modul1test.dao.KulManDAO;
import jwd4modul1test.model.Grad;
import jwd4modul1test.model.KulturnaManifestacija;
import jwd4modul1test.util.PomocnaKlasa;

public class GradUI {
	public static Connection conn = ApplicationUI.getConn();

	public static void prikazGradova() {
		ArrayList<Grad> gradovi = new ArrayList<>();
		gradovi = GradDAO.getAll(conn);
		System.out.println();
		for (Grad grad : gradovi) {

			System.out.println(grad.toString());
		}
	}

	public static void izmenaGrada() {
		System.out.println("Unesite ID broj grada koji želite da promenite:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		Grad grad = GradDAO.getByID(conn, id);
		if (grad != null) {
			System.out.println(grad.toString());
			System.out.println();
			System.out.println("Unesite novo ime za grad:");
			String ime = PomocnaKlasa.ocitajTekst();
			System.out.println("Unesite novi ptt broj:");
			int ptt = PomocnaKlasa.ocitajCeoBroj();
			grad = new Grad(id, ime, ptt);
			if (GradDAO.update(conn, grad) == true) {
				System.out.println("Podaci su uspešno promenjeni");
			} else
				System.out.println("Došlo je do problema sa promenom ipodataka");
		} else
			System.out.println("Ne postoji grad sa unesenim ID");

	}

	public static void brisanjeGrada() {
		System.out.println("Unesite id broj grada kog želite da izbrišete:");
		int id = PomocnaKlasa.ocitajCeoBroj();
		// Pošto je grad referenciran u kulturnim manifestacijama prvo brišemo sve
		// kulturne manifestacije datim gradom, pa tek onda brišemo i sam grad
		ArrayList<KulturnaManifestacija> kM = KulManDAO.getAll(conn);
		for (KulturnaManifestacija kultMan : kM) {
			if(kultMan.getGrad().getId()==id) {
				KulManDAO.delete(conn, kultMan.getId());
			}
		}
		if (GradDAO.delete(conn, id) == true) {
			System.out.println("Uspešno ste izbrisali grad");
		} else
			System.out.println("Bilo je problema tokom brisanja grada?");
	}

}
