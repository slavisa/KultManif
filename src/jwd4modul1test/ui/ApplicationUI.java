package jwd4modul1test.ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import jwd4modul1test.util.PomocnaKlasa;

public class ApplicationUI {

private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/test?useSSL=false", //TODO zameni naziv baze podataka
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}
	
	public static void main(String[] args)  {
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				GradUI.prikazGradova();
				break;
			case 2:
				KulManUI.PrikazManif();
				break;
			case 3:
				KulManUI.pretragaPoID();
				break;
			case 4:
				KulManUI.izmenaNaziva();
				break;
			case 5:
				KulManUI.unosManif();
				break;
			case 6:
				KulManUI.brisanjeManif();
				break;
			case 7:
				KulManUI.izmenaSvihPod();
				break;
			case 8:
				GradUI.izmenaGrada();
				break;
			case 9:
				GradUI.brisanjeGrada();
				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturne manifestacije / gradovi - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - prikaz gradova");
		System.out.println("\tOpcija broj 2 - prikaz kulturnih manifestacija");
		System.out.println("\tOpcija broj 3 - pretraga kulturne maifestacije po id broju");
		System.out.println("\tOpcija broj 4 - izmena naziva manifestacije");
		System.out.println("\tOpcija broj 5 - unos manifestacije");
		System.out.println("\tOpcija broj 6 - brisanje manifestacije");
		System.out.println("\tOpcija broj 7 - izmena manifestacije ");
		System.out.println("\tOpcija broj 8 - izmena grada ");
		System.out.println("\tOpcija broj 9 - brisanje grada ");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}
}
