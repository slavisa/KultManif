package jwd4modul1test.model;

public class Grad {
	
	protected int id;
	protected String naziv;
	protected int ptt;
	
	public Grad() {}

	public Grad(int id, String naziv, int ptt) {
		this.id = id;
		this.naziv = naziv;
		this.ptt = ptt;
	}

	@Override
	public String toString() {
		return "Grad [id=" + id + ", naziv=" + naziv + ", ptt=" + ptt + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getPtt() {
		return ptt;
	}

	public void setPtt(int ptt) {
		this.ptt = ptt;
	}
	

}
